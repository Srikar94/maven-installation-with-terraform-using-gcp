provider "google" {
	credentials = "${file("")}"
	project = ""
	region = ""
}

 
resource "random_id" "instance_id" {
byte_length = 8
}
 
resource "google_compute_instance" "nucuta" {
name         = "nucuta-vm-${random_id.instance_id.hex}"
machine_type = "f1-micro"
zone         = "asia-south1-a"
 
boot_disk {
	initialize_params {
		image = "ubuntu-os-cloud/ubuntu-1604-lts"
	}
}

metadata_startup_script = "sudo apt-get update && sudo apt install maven -y"

network_interface {
network = "default" 
access_config {

}
}

metadata = {
sshKeys = "srikarsai:${file("temp.pub")}"
}
}
 
resource "google_compute_firewall" "default" {
name    = "maven-firewall"
network = "default"
 
allow {
protocol = "tcp"
ports    = ["8080"]
}
 
allow {
protocol = "icmp"
}
}

output "ip" {
value = "${google_compute_instance.nucuta.network_interface.0.access_config.0.nat_ip}"
}

